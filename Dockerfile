FROM rust:latest
MAINTAINER elois <c@elo.tf>
LABEL version="1.70.0"
LABEL description="CI for Rust projects"

# Add cmake
RUN apt-get update; \
   apt-get install -y cmake

# Install fmt and clippy
RUN rustup component add rustfmt && rustup component add clippy

# install cargo-deb, cargo-deny, cargo-tarpaulin and conventional_commits_linter
RUN cargo install cargo-deb && cargo install cargo-deny && cargo install cargo-tarpaulin && cargo install conventional_commits_linter --version ^0
